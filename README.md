[![crates.io](https://img.shields.io/crates/v/systemstat.svg)](https://crates.io/crates/get_sys_info)
[![API Docs](https://docs.rs/systemstat/badge.svg)](https://docs.rs/get_sys_info/)
[![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

# get_sys_info

get system information:
| Information      | Description |
| ----------- | ----------- |
| CPU load      | CPU load info|
| load average| Text|
|memory usage|get real time memory usage|
|uptime / boot time|get system bootup time|
|battery life|get battery life info|
|filesystem mounts (and disk usage)|get filesystem mounts ,total_space, available_space|
|disk I/O statistics|get disk io info|
|network interfaces|get network interfaces info|
|network traffic statistics|get network statistic info|
|CPU temperature|get CPU useage|


|  Supported platforms  | 
| ----------- | 
|FreeBSD|
|Linux|
|OpenBSD|
|Windows|
|macOS|


## example
```rust
use get_sys_info::{saturating_sub_bytes, Platform, System};
use std::thread;
use std::time::Duration;

fn main() {
    let sys = System::new();

    {
        let mounts = sys.mounts().unwrap_or_else(|r| {
            println!("get mounts error:{}", r);
            vec![]
        });
        for mount in mounts.iter() {
            println!(
                "[mounted_from:{}, file_type:{},mounted_at:{},available:{},total:{}]",
                mount.fs_mounted_from, mount.fs_type, mount.fs_mounted_on, mount.avail, mount.total
            );
        }
    }
    {
        match sys.mount_at("/") {
            Ok(mount) => {
                println!(
                    "[mounted_from:{}, file_type:{},mounted_at:{},available:{},total:{}]",
                    mount.fs_mounted_from,
                    mount.fs_type,
                    mount.fs_mounted_on,
                    mount.avail,
                    mount.total
                );
            }
            Err(x) => println!("\n mount_at /: error: {}", x),
        }
    }
    {
        let stats = sys.block_device_statistics().unwrap_or_else(|r| {
            println!("get block_device_statistics error error:{}", r);
            std::collections::BTreeMap::new()
        });
        for blkstats in stats.values() {
            println!("{}: {:?}", blkstats.name, blkstats);
        }
    }
    {
        let netifs = sys.networks().unwrap_or_else(|r| {
            println!("\n networks: error: {}", r);
            std::collections::BTreeMap::new()
        });
        for netif in netifs.values() {
            println!(
                "{} statistics: ({:?})",
                netif.name,
                sys.network_stats(&netif.name)
            );
        }
    }
    {
        match sys.battery_life() {
            Ok(battery) => print!(
                "\nBattery: {}%, {}h{}m remaining",
                battery.remaining_capacity * 100.0,
                battery.remaining_time.as_secs() / 3600,
                battery.remaining_time.as_secs() % 60
            ),
            Err(x) => print!("\n battery: error: {}", x),
        }
    }
    {
        match sys.on_ac_power() {
            Ok(power) => println!(", AC power: {}", power),
            Err(x) => println!(", AC power: error: {}", x),
        }
    }
    {
        match sys.memory() {
            Ok(mem) => println!(
                "\n memory: {} used / {} ({} bytes) total ({:?})",
                saturating_sub_bytes(mem.total, mem.free),
                mem.total,
                mem.total.as_u64(),
                mem.platform_memory
            ),
            Err(x) => println!("\n memory: error: {}", x),
        }
    }
    {
        match sys.load_average() {
            Ok(loadavg) => println!(
                "\n load_average: {} {} {}",
                loadavg.one, loadavg.five, loadavg.fifteen
            ),
            Err(x) => println!("\n load_average: error: {}", x),
        }
    }
    {
        match sys.uptime() {
            Ok(uptime) => println!("\n uptime: {:?}", uptime),
            Err(x) => println!("\n uptime: error: {}", x),
        }
    }
    {
        match sys.boot_time() {
            Ok(boot_time) => println!("\n boot_time: {}", boot_time),
            Err(x) => println!("\n boot_time: error: {}", x),
        }
    }
    {
        match sys.cpu_load_aggregate() {
            Ok(cpu) => {
                thread::sleep(Duration::from_secs(1));
                let cpu = cpu.done().unwrap();
                println!(
                    "CPU load: {}% user, {}% nice, {}% system, {}% intr, {}% idle ",
                    cpu.user * 100.0,
                    cpu.nice * 100.0,
                    cpu.system * 100.0,
                    cpu.interrupt * 100.0,
                    cpu.idle * 100.0
                );
            }
            Err(x) => println!("\n cpu_load_aggregate: error: {}", x),
        }
    }
    {
        match sys.cpu_temp() {
            Ok(cpu_temp) => println!("\n cpu_temp: {}", cpu_temp),
            Err(x) => println!("\n cpu_temp: {}", x),
        }
    }
    {
        match sys.socket_stats() {
            Ok(stats) => println!("\nsocket_stats: {:?}", stats),
            Err(x) => println!("\nsocket_stats: error: {}", x),
        }
    }
}

```
